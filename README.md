# Carte Mémoire avec Éditeur de cartes #

## Description :##
Carte Mémoire est un jeu sur le principe du "Memory" (cf : [Memory (Wikipedia)]( http://fr.wikipedia.org/wiki/Memory_%28jeu%29)).
Trois modes de jeux sont disponibles : 

* un joueur
* deux joueurs
* un joueur et un ordinateur (difficulté ajustable)

Il est accompagné d'un éditeur permettant de créer ses propres cartes afin de personnaliser le jeu.

*Capture d'écran du jeu au cours d'une partie*

![image1.png](https://bitbucket.org/repo/xMn8xp/images/459164164-image1.png)

*Capture d'écran de l'éditeur de cartes*

![image2.png](https://bitbucket.org/repo/xMn8xp/images/3562823922-image2.png)

## Installation du jeu : ##
Le dépôt contient les deux exécutables permettant de lancer le jeu et l'éditeur ainsi que le dossier contenant les images.

Un installateur est disponible dans "Téléchargements".

## Informations techniques : ##
Le jeu et l'éditeur ont été développés en Java.